package com.advancedpwr.loft.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.advancedpwr.loft.slf4j.UnitTestingLogger;
import com.advancedpwr.loft.slf4j.UnitTestingLoggerFactory;

class ClassThatLogsTest
{

	@Test
	void test()
	{
		// Static therefore no parallel tests:
		UnitTestingLogger logger = UnitTestingLoggerFactory.getLogger();

		ClassThatLogs clazz = new ClassThatLogs( 10, false );

		logger.expectWarn( "Be careful! This method is magic." );
		logger.expectInfo( "Have fun!" );
		clazz.logMagic();

		logger.expectDebug( "This bean weighs {} grams, and it's {} that it is moldy.",
				clazz.getGrams(), clazz.isMoldy() );
		clazz.logStats();

		System.out.println( "EXPECTED:\n" + logger.expected() + "\n\n" );
		System.out.println( "ACTUALS:\n" + logger.actuals() + "\n\n" );
		assertEquals( logger.expected(), logger.actuals() );
	}

}
