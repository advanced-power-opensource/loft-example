package com.advancedpwr.loft.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassThatLogs
{

	private static final Logger LOGGER = LoggerFactory.getLogger( ClassThatLogs.class );

	protected int fieldGrams;
	protected boolean fieldMoldy;

	public ClassThatLogs( int grams, boolean isMoldy )
	{
		setGrams( grams );
		setMoldy( isMoldy );
	}

	public void logMagic()
	{
		LOGGER.warn( "Be careful! This method is magic." );
		LOGGER.info( "Have fun!" );
	}

	public void logStats()
	{
		LOGGER.debug( "This bean weighs {} grams, and it's {} that it is moldy.", getGrams(),
				isMoldy() );
	}

	public int getGrams()
	{
		return fieldGrams;
	}

	public void setGrams( int grams )
	{
		fieldGrams = grams;
	}

	public boolean isMoldy()
	{
		return fieldMoldy;
	}

	public void setMoldy( boolean moldy )
	{
		fieldMoldy = moldy;
	}
}